export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyDOzPn22W26x5JkEETeg7q_PH49nwEbXC8",
    authDomain: "fosil-pwa.firebaseapp.com",
    databaseURL: "https://fosil-pwa.firebaseio.com",
    projectId: "fosil-pwa",
    storageBucket: "fosil-pwa.appspot.com",
    messagingSenderId: "39969959389",
    appId: "1:39969959389:web:a85d7c1ddebe69e0a65a34",
    measurementId: "G-0LGRC60CEH"
  },
  keyGoogleMaps:'AIzaSyCuvotkszfsyv99NOeb4pNYu5bDfjaw_Ww',
  email:{
    to:'fosil@fosil.cz',
    hint:'Nová objednávka',
		subject:'Nová objednávka v aplikaci',
		html:'<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>New Order</title></head><body><h2>Nová objednávka v aplikaci</h2></body></html>',
  }
};
