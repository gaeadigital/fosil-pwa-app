import { Content } from './content';
import { environment } from '../../environments/environment';
export class Order {
	id?: string = null;
	orderNumber:number = null;
	address: string = null;
	phoneNumber: string = null;
	paymentType: string = null;
	paymentStatus: string = null;
	status: string = null;
	totalAmount: number = null;
	deliveryDistance?:any = null;
	deliveryAmount: number = null;
	timestampReceived: Date = null;
	timestampRecord: Date = null;
	customerPickUp:boolean = false;
	to = environment.email.to;
	cc:string = null;
	message = {
		hint:environment.email.hint,
		subject:environment.email.subject,
		html:environment.email.html
	};

	order?:Array<Content>;
	isCanceled?:boolean = false;

	constructor(){
		this.totalAmount = 0;
		this.deliveryAmount = 35;
		this.status = 'NEW';
		this.order = new Array<Content>();
	}
}
