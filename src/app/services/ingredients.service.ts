import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { GroupIngredient } from '../entity/group-ingredient';
import { Ingredient } from '../entity/ingredient';

@Injectable({
  providedIn: 'root'
})
export class IngredientsService {

  groupIngredientCollectionName='group-ingredient';
  ingredientCollectionName='ingredients';

  constructor(private afs: AngularFirestore) { }

  async getGroups() {
    return await this.afs.collection(this.groupIngredientCollectionName, ref=>ref.orderBy('order','asc')).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as GroupIngredient;
        const id = a.payload.doc.id;
        return { id, ...data };
      })));
  }

  async getIngredientsByGroup(groupId:string){
    return await this.afs.collection(this.ingredientCollectionName, 
      ref=>ref.where('group','==',groupId).orderBy('order','asc')).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Ingredient;
        const id = a.payload.doc.id;
        return { id, ...data };
      })));
  }
  
}
