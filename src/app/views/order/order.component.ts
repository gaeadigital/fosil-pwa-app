import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppDataService } from '../../services/app-data.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  lang: string;
  heightWindows = 500;

  totalAmount: number;

  constructor(public appDataService: AppDataService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router) {
    this.heightWindows = window.innerHeight;
    this.totalAmount = 0;
  }

  ngOnInit() {
    this.lang = localStorage.getItem('selectedLang');
    const fullOrder = localStorage.getItem('fullOrder');
    if(fullOrder){
      this.appDataService.fullOrder = JSON.parse(fullOrder);
      this.sumAmount();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.heightWindows = event.target.innerHeight;
  }

  toggleMenu(){
    if(this.appDataService.sideMenuWidth === 0){
      this.appDataService.sideMenuWidth = 350;
    } else {
      this.appDataService.sideMenuWidth = 0;
    }
  }

  changeLang(selectedLang){
    this.lang = selectedLang;
    this.appDataService.loadLabelsByLang(selectedLang);
    this.toggleMenu();
  }

  newItem(){
    this._router.navigate(['/choose']);
  }

  sumAmount(){
    this.appDataService.fullOrder.totalAmount = 0;
    this.appDataService.fullOrder.order.forEach(order =>{
      this.appDataService.fullOrder.totalAmount+= order.amount;
    });
  }

  removeItem(idx:number){
    this.appDataService.fullOrder.order.splice(idx,1);
    localStorage.setItem('fullOrder', JSON.stringify(this.appDataService.fullOrder));
    this.sumAmount();
  }

  confirmOrder(){
    localStorage.setItem('fullOrder', JSON.stringify(this.appDataService.fullOrder));
    this._router.navigate(['/checkout']);
  }

  cancelOrder(){
    this._router.navigate(['/new-order/'+this.lang]);
  }
}
