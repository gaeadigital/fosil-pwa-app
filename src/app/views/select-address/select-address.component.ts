import { Component, OnInit, ViewChild, NgZone, ElementRef } from '@angular/core';
import { AppDataService } from '../../services/app-data.service';
import { Router } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-select-address',
  templateUrl: './select-address.component.html',
  styleUrls: ['./select-address.component.css']
})
export class SelectAddressComponent implements OnInit {

  @ViewChild('addressList', null) addressList: ElementRef;

  heightWindows = 500;
  lang: string;
  markers: marker[];

  search: string;
  title: string = 'FosilApp';
  fosilLongitude = 14.4058744;
  fosilLatitude = 50.046914;
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  addreses: any;

  goList: boolean = false;
  private geoCoder;

  constructor(public appDataService: AppDataService,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) {
    this.heightWindows = window.innerHeight;

    this.setCurrentLocation();
    this.markers = [];
  }

  ngOnInit() {
    this.latitude = this.fosilLatitude;
    this.longitude = this.fosilLongitude;
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {

      this.geoCoder = new google.maps.Geocoder();

    }).catch(err => {
      console.log(err);
    });
  }

  backOrder() {
    this.router.navigate(['checkout']);
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }


  markerDragEnd($event) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress();
  }

  mapClicked($event) {
    console.log($event);
    if ($event !== null) {
      this.markers = [];
      this.markers.push({
        lat: $event.coords.lat,
        lng: $event.coords.lng,
        draggable: true
      });
      this.latitude = $event.coords.lat;
      this.longitude = $event.coords.lng;
      this.getAddress();
    }
  }

  async getAddress() {
    const getAddressByCoords = () =>{
      return new Promise((resolve, reject) => {
        this.geoCoder.geocode({ 'location': { lat: this.latitude, lng: this.longitude } }, (results, status) => {
          console.log(results);
          console.log(status);
          if (status === 'OK') {
            const filtered = [];
            results.forEach(result => {
              if(result.types.includes('street_address')){
                filtered.push(result);
              }
            });
            resolve(filtered);
          } else {
           reject('Empty list');
          }
        });
      });
    };
    this.addreses =  await getAddressByCoords();
    console.log(this.addreses);
  }

  async searchAddressByLine(address) {
    const getAddress = address => {
      return new Promise((resolve, reject) => {
        const geocoder = new google.maps.Geocoder();
        geocoder.geocode({ address: address }, (results, status) => {
          if (status === 'OK') {
            resolve(results);
          } else {
            resolve(status);
          }
        });
      });
    };
    this.addreses = await getAddress(address);
    console.log(this.addreses)
  }
}
// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}