import { Component, OnInit, HostListener } from '@angular/core';
import { IngredientsService } from '../../services/ingredients.service';
import { GroupIngredient } from '../../entity/group-ingredient';
import { Observable } from 'rxjs';
import { AppDataService } from '../../services/app-data.service';
import { Ingredient } from '../../entity/ingredient';
import { Router } from '@angular/router';
import { Content } from '../../entity/content';

@Component({
  selector: 'app-choose-ingredients',
  templateUrl: './choose-ingredients.component.html',
  styleUrls: ['./choose-ingredients.component.css']
})
export class ChooseIngredientsComponent implements OnInit {

  heightWindows = 500;
  groups:Array<GroupIngredient>;

  burritoIngredients:Array<Ingredient>;

  totalAmount:number;

  sideMenuWidth:number;

  langSelected;

  constructor(public appDataService:AppDataService,private ingredientsService: IngredientsService, private router:Router) {
    this.heightWindows = window.innerHeight;
    this.totalAmount = 0;
    this.sideMenuWidth = 0;
    this.langSelected = localStorage.getItem('selectedLang');
   }

  ngOnInit() {
    //console.log(this.appDataService.labels.length);
    if(this.appDataService.labels.length === 0){
      const labels = JSON.parse(localStorage.getItem('labels'));
      labels.forEach(label => {
        this.appDataService.labels[label.id] = label[this.langSelected];
      });
    }
    this.groups = new Array<GroupIngredient>();
    this.burritoIngredients = new Array<Ingredient>();
    //Obtenemos los grupos de ingredientes
    this.ingredientsService.getGroups().then((groupsSnapshot) => {
      groupsSnapshot.forEach((groupDocs) => {
        groupDocs.forEach((groupDoc)=>{
          groupDoc.ingredients = new Array<Ingredient>();
          //Obtenemos los ingredientes.
          this.ingredientsService.getIngredientsByGroup(groupDoc.id).then((ingredientSnapshot)=>{
            ingredientSnapshot.forEach((ingredientsDocs) =>{
              ingredientsDocs.forEach((ingredientsDoc)=> {
                if(ingredientsDoc.check){
                  this.addIngredient(ingredientsDoc);
                }
                groupDoc.ingredients.push(ingredientsDoc);
                this.sumAmount();
              });
            });
          });

          this.groups.push(groupDoc);
        });
      });
    });
  }

  toggleMenu(){
    console.log('toogle');
    if(this.appDataService.sideMenuWidth === 0){
      this.appDataService.sideMenuWidth = 350;
    } else {
      this.appDataService.sideMenuWidth = 0;
    }
  }

  @HostListener('window:resize', [ '$event' ])
	onResize(event) {
		this.heightWindows = event.target.innerHeight;
  }

  toggleIngredient(ingredient:Ingredient){
    const index = this.getIngredientIndex(ingredient);
    if(ingredient.check){
      //Quita el ingredient
      ingredient.check = false;
      this.burritoIngredients.splice(index,1);
    } else {
      //Agrega el ingrediente.
      if(index===-1){
        ingredient.check = true;
        this.addIngredient(ingredient);
      }
    }
    this.sumAmount();
    console.log(this.burritoIngredients);
  }

  addIngredient(ingredient:Ingredient){
    this.burritoIngredients.push(ingredient);
    
  }

  getIngredientIndex(ingredient:Ingredient){
    let indexIngredient = -1;

    for (let index = 0; index < this.burritoIngredients.length; index++) {
      const element = this.burritoIngredients[index];
      if(ingredient.id === element.id){
        indexIngredient = index;
      }
    }

    return indexIngredient;
  }

  sumAmount(){
    this.totalAmount = 0;
    this.burritoIngredients.forEach(ingredient=>{
      this.totalAmount += ingredient.amount;
    });
  }

  addBurritoToOrder(){
    const fullOrder = localStorage.getItem('fullOrder');
    if(fullOrder){
      this.appDataService.fullOrder = JSON.parse(fullOrder);
    }
    const content = new Content();
    content.contents = this.burritoIngredients;
    content.amount = this.totalAmount;
    this.appDataService.fullOrder.order.push(content);
    localStorage.setItem('fullOrder', JSON.stringify(this.appDataService.fullOrder));
    this.router.navigate(['/order']);
  }

  cancelSelect(){
    this.router.navigate(['/order']);
  }
  
}
