import { Ingredient } from './ingredient';

export class Content {

    amount:number= null;
    contents:Array<Ingredient>;
    
}
