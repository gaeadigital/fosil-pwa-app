import { Injectable } from '@angular/core';
import { Lang } from '../entity/lang';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { Label } from '../entity/label';

@Injectable({
  providedIn: 'root'
})
export class LabelsService {
  
  langCollectionName = 'language';
  labelsCollectionName = 'labels';

  constructor(private afs: AngularFirestore) { }


  async getAvailableLang(){
    return await this.afs.collection<Lang>(this.langCollectionName).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Lang;
        const id = a.payload.doc.id;
        return { id, ...data };
      })));
  }

  async getLabels()  {
    return await this.afs.collection(this.labelsCollectionName).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Label;
        const id = a.payload.doc.id;
        return { id, ...data };
      })));
  }
}
