import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChooseIngredientsComponent } from './views/choose-ingredients/choose-ingredients.component';
import { NewOrderComponent } from './views/new-order/new-order.component';
import { OrderComponent } from './views/order/order.component';
import { CheckoutComponent } from './views/checkout/checkout.component';
import { SelectAddressComponent } from './views/select-address/select-address.component';


const routes: Routes = [
  { path: 'choose', component: ChooseIngredientsComponent },
  { path: 'new-order/:lang', component: NewOrderComponent },
  { path: 'order', component: OrderComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'select-address', component: SelectAddressComponent },
  { path: '',   redirectTo: '/new-order/cs-CZ', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
