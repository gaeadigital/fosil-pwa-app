import { Component, OnInit, HostListener } from '@angular/core';
import { AppDataService } from '../../services/app-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'src/app/entity/order';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.css']
})
export class NewOrderComponent implements OnInit {

  lang: string;
  heightWindows = 500;

  loading:boolean = true;

  mainBackground="https://firebasestorage.googleapis.com/v0/b/fosil-pwa.appspot.com/o/settings%2Fbackground%2FburritoFosil.jpg?alt=media&token=b515939e-ac9f-4c0c-b194-bf245cfef062";

  latitude: number;
  longitude: number;
  zoom:number;

  constructor(public appDataService: AppDataService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router) {
    this.heightWindows = window.innerHeight;
  }

  ngOnInit() {
    this.appDataService.loadAvailableLang();
    this.lang = this._activatedRoute.snapshot.params['lang'];
    this.appDataService.selectedLang = this.lang;
    this.appDataService.loadLabelsByLang(this.lang);
    this.appDataService.loadSettings();
    this.heightWindows = window.innerHeight;
    localStorage.setItem('selectedLang', this.lang);
    this.setCurrentLocation();
    setTimeout( () => {
      this.mainBackground = this.appDataService.mainBg;
      this.loading = false;
    }, 3000);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.heightWindows = event.target.innerHeight;
  }

  newOrder(langSended) {
    this.appDataService.fullOrder = new Order();
    localStorage.setItem('fullOrder', JSON.stringify(this.appDataService.fullOrder));
    this.appDataService.selectLang(langSended);
    this._router.navigate(['/choose']);
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        const geolocation = {
          'latitude': this.latitude,
          'longitude':this.longitude,
          'zoom:':10
        };
        localStorage.setItem('geolocation',JSON.stringify(geolocation));
        this.zoom = 10;
      });
    }
  }
}
