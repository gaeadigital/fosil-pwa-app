import { Injectable } from '@angular/core';
import { Lang } from '../entity/lang';
import { LabelsService } from './labels.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from '../entity/order';
import { Setting } from '../entity/setting';
import { OrderService } from './order.service';

@Injectable({
	providedIn: 'root'
})
export class AppDataService {

	selectedLang: string;
	actualOrder: string;
	openSideMenu: boolean = false;

	labels = [];
	langs: Array<Lang>;

	settings: Array<Setting>;

	user: firebase.User = null;

	fullOrder: Order;

	sideMenuWidth: number = 0;
	mainBg:string;


	constructor(private _labelsService: LabelsService, private _route: Router,
		private _activatedRoute: ActivatedRoute, private _orderService:OrderService) {
		this.selectedLang = this._activatedRoute.snapshot.params['lang'];
	}

	loadAvailableLang() {
		this._labelsService.getAvailableLang().then((langs) => {
			langs.forEach((lang) => {
				this.langs = lang;
			});
		});
	}

	selectLang(lang: string) {
		console.log(lang);
		this.loadLabelsByLang(lang);
	}

	/**Obtenemos las etiquetas dependiendo el lenguaje seleccionado. */
	async loadLabelsByLang(lang: string) {
		await this._labelsService.getLabels().then((labelsSnapshot) => {
			labelsSnapshot.forEach((labels) => {
				localStorage.setItem('labels', JSON.stringify(labels));
				labels.forEach((label) => {
					this.labels[label.id] = label[lang];
				});
			});
		});
		console.log(this.labels);
	}

	async loadSettings(){
		await this._orderService.getSettings().then((settingSnapshot) => {
			settingSnapshot.forEach((settings) =>{
				this.settings = settings;
				settings.forEach(setting => {
					if(setting.id === 'mainpage.background'){
					  this.mainBg = setting.path;
					}
				  });
			});
		});
	}

}
