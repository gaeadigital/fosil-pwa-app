import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Distance } from '../entity/distance';

@Injectable({
  providedIn: 'root'
})
export class DistancesService {

  distanceCollectionName:string ='distances';

  constructor(private afs:AngularFirestore) { }

  getDistances(){
    return this.afs.collection(this.distanceCollectionName, ref=> ref.orderBy('amount','desc')).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Distance
        const id = a.payload.doc.id;
        return { id, ...data };
      })));
  }
}
