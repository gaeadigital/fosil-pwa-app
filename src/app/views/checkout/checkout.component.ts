import { Component, OnInit, HostListener } from '@angular/core';
import { AppDataService } from '../../services/app-data.service';
import { Router } from '@angular/router';
import { OrderService } from '../../services/order.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  heightWindows = 500;
  lang:string;

  sendedOrder:boolean = false;
  validData:boolean = false;

  deliveryService:boolean = true;

  options = {  year: 'numeric', month: 'short', day: 'numeric' , hour:'numeric', minute:'numeric', hour12: false };

  constructor(public appDataService:AppDataService,
    private router:Router, private _orderService:OrderService) {
    this.heightWindows = window.innerHeight;
   }

  ngOnInit() {
    this.lang = localStorage.getItem('selectedLang');
    const fullOrder = localStorage.getItem('fullOrder');
    if(fullOrder){
      this.appDataService.fullOrder = JSON.parse(fullOrder);
      console.log(this.appDataService.fullOrder);
    }
  }

  @HostListener('window:resize', [ '$event' ])
	onResize(event) {
    this.heightWindows = event.target.innerHeight;
    console.log(this.heightWindows);
  }

  backOrder(){
    this.router.navigate(['order']);
  }

  searchAddress(){
    this.router.navigate(['select-address']);
  }

  toggleDelivery(){
    if(this.deliveryService){
      this.deliveryService = false;
    } else {
      this.deliveryService = true;
    }
  }

  prepareOrderContent(){
    let content = '';

    //Iteramos la orden para generar el contenido
    this.appDataService.fullOrder.order.forEach(order => {
      
      let detail = '';
      order.contents.forEach(ingredient => {
        detail+= `<span style="font-size:10px; color:#666;">${this.appDataService.labels[ingredient.label]}, </span>`;
      });
      content+=`<tr>
        <td>
          <h5 style="margin:0px;">Burrita</h5>
          ${detail}
        </td>
        <td style="text-align: right;">
          Kč  ${order.amount}
        </td>
      </tr>`;
    });

    return content;
  }

  buildEmailContent(){
    return `<div style="max-width: 450px;margin: auto !important;padding: 1em; border:solid 1px #ddd;border-radius:10px;">
      <table style="width:100%">
        <thead>
            <tr>
              <th colspan="2" style="text-align: right;">
                <img style="width:100px"  src="https://firebasestorage.googleapis.com/v0/b/fosil-pwa.appspot.com/o/settings%2Flogos%2FLogo2.jpg?alt=media&token=b28964ab-e5d8-4f51-a783-d4d4815b4dad">
              </th>
            </tr>
            <tr>
                <th style="text-align: left;">
                    <h3>New Order | Nová objednávka</h3>
                </th>
                <th style="text-align: right;">
                    <label style="font-size:10px;color:#666;">Received | Obdržel</label>
                    <p style="margin:0;">${this.appDataService.fullOrder.timestampReceived.toLocaleDateString("cs-CZ",this.options)}</p>
                </th>
            </tr>
        </thead>
        <tbody>
          
            <tr>
                <td colspan="2" style="padding-bottom:2rem">
                    <label style="font-size:10px;color:#666;">Address | Adresa</label>
                    <p style="margin:0;">${this.appDataService.fullOrder.address}</p>
                    <label style="font-size:10px;color:#666;">Phone number | Telefonní číslo</label>
                    <p style="margin:0;">${this.appDataService.fullOrder.phoneNumber}</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                  <hr>
                <td>
              </tr>
            ${this.prepareOrderContent()}
            <tr>
                <td colspan="2">
                  <hr>
                <td>
              </tr>
            <tr>
                <td colspan="2" >
                    <h4>Bill | Účtovat</h4>
                </td>
            </tr>
            <tr>
                <td>
                    Order | Objednat: ${this.appDataService.fullOrder.order.length} burrito(s)
                </td>
                <td style="text-align: right;">
                    Kč ${this.appDataService.fullOrder.totalAmount}
                </td>
            </tr>
            <tr>
                <td>
                    Delivery | Dodávka: ( ${this.appDataService.fullOrder.deliveryDistance} km)
                </td>
                <td style="text-align: right;">
                    Kč ${this.appDataService.fullOrder.deliveryAmount}
                </td>
            </tr>
            <tr>
              <td colspan="2">
              <hr>
              </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <h4>Total | Celkový<h4>
                </td>
                <td style="text-align: right;">
                    <h4>Kč ${this.appDataService.fullOrder.totalAmount + this.appDataService.fullOrder.deliveryAmount}</h4>
                </td>
            </tr>
        </tfoot>
    </table>
    <div>`;
  }

  sendOrder(){
    if(!this.deliveryService){
      this.appDataService.fullOrder.address = this.appDataService.labels['store.address'];
      this.appDataService.fullOrder.deliveryDistance = 0;
      this.appDataService.fullOrder.deliveryAmount = 0;
      this.appDataService.fullOrder.customerPickUp=true;
    }
    if(this.appDataService.fullOrder.address === null || this.appDataService.fullOrder.address === ''){
      this.validData = true;
    } else  if(this.appDataService.fullOrder.phoneNumber === null || this.appDataService.fullOrder.phoneNumber === ''){
      this.validData = true;
      
    } else  if(this.appDataService.fullOrder.cc === null || this.appDataService.fullOrder.cc === ''){
      this.validData = true;
    } else {
      this.validData = false;
      this.appDataService.fullOrder.timestampRecord = new Date(); 
      this.appDataService.fullOrder.timestampReceived = new Date();
      
      this.appDataService.fullOrder.message.html = this.buildEmailContent();
      
      this._orderService.saveOrder(this.appDataService.fullOrder).then(success=> {
        this.sendedOrder = true;
        this.validData = false;
        setTimeout(function(){ 
          this.window.location = '/';
        }, 3000);
      }).catch(err =>{
        console.log(err);
      });
    }
    console.log(this.validData);
  }
}
