import { Component, OnInit, HostListener, Input } from '@angular/core';
import { AppDataService } from '../../services/app-data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  lang: string;
  heightWindows = 500;

  totalAmount: number;

  constructor(public appDataService: AppDataService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router) {
      this.heightWindows = window.innerHeight;
      this.totalAmount = 0;
     }

  ngOnInit() {
    this.lang = localStorage.getItem('selectedLang');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.heightWindows = event.target.innerHeight;
  }

  toggleMenu(){
    console.log('toogle');
    if(this.appDataService.sideMenuWidth === 0){
      this.appDataService.sideMenuWidth = 350;
    } else {
      this.appDataService.sideMenuWidth = 0;
    }
  }

  changeLang(selectedLang){
    this.lang = selectedLang;
    localStorage.setItem('selectedLang',selectedLang);
    this.appDataService.loadLabelsByLang(selectedLang);
    this.toggleMenu();
  }
}
