export class Ingredient {


    id?:string= null;
    label:string= null;
    group:string= null;
    amount:number= null;
    order:number= null;

    check:boolean = false;

    isSelected?:boolean = false;
}
