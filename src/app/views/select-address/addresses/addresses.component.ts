import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { AppDataService } from '../../../services/app-data.service';
import { DistancesService } from '../../../services/distances.service';
import { Distance } from '../../../entity/distance';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.css']
})
export class AddressesComponent implements OnInit {

  @Input() addressList:any;
  @Input() origenLat:any;
  @Input() origenLong:any;
  @Input() fosilLat:any;
  @Input() fosilLong:any;


  private geoCoder;

  distances: Array<Distance>;

  constructor(public appDataService:AppDataService, private _distancesService:DistancesService,
    private _router:Router) { 
     console.log(this.addressList);
    }

  ngOnInit() {
    this._distancesService.getDistances().subscribe(result => {
      this.distances = result;
      this.geoCoder = new google.maps.Geocoder;
    });
  }

  selectAddress(address){
    console.log(address);
    this.appDataService.fullOrder.address = address.formatted_address;
    let distance = this.getKilometros(address.geometry.location.lat(),address.geometry.location.lng(),this.fosilLat,this.fosilLong);
    const total = Number.parseFloat(distance) *1000;
    console.log(total);
    this.distances.forEach(distan=>{
      console.log(distan);
      if(total < distan.id){
        console.log(distan.id);
        this.appDataService.fullOrder.deliveryDistance = distance;
        this.appDataService.fullOrder.deliveryAmount =distan.amount;
      }
    });
    localStorage.setItem('fullOrder',JSON.stringify(this.appDataService.fullOrder));
    this._router.navigate(['/checkout']);
  }

  getKilometros = function(lat1,lon1,lat2,lon2)
  {
      let rad = function(x) {return x*Math.PI/180;}
      var R = 6378.137; //Radio de la tierra en km
      var dLat = rad( lat2 - lat1 );
      var dLong = rad( lon2 - lon1 );
      var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = R * c;
      return d.toFixed(3); //Retorna tres decimales
  }

  findAddressByCoords(){
    this.addressList = [];
    console.log(this.origenLat+' - '+this.origenLong);
    this.geoCoder.geocode({ 'location': { lat: this.origenLat, lng: this.origenLong } }, (results, status) => {
      console.log(results);
      results.forEach(addressLine => {
        this.addressList.push(addressLine);
      });
    });
  }

}
