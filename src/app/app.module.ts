import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { AgmCoreModule } from '@agm/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewOrderComponent } from './views/new-order/new-order.component';
import { ChooseIngredientsComponent } from './views/choose-ingredients/choose-ingredients.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AppDataService } from './services/app-data.service';
import { LabelsService } from './services/labels.service';
import { OrderComponent } from './views/order/order.component';
import { CheckoutComponent } from './views/checkout/checkout.component';
import { SelectAddressComponent } from './views/select-address/select-address.component';
import { AddressesComponent } from './views/select-address/addresses/addresses.component';
import { SideMenuComponent } from './views/side-menu/side-menu.component';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
  declarations: [
    AppComponent,
    NewOrderComponent,
    ChooseIngredientsComponent,
    OrderComponent,
    CheckoutComponent,
    SelectAddressComponent,
    AddressesComponent,
    SideMenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    HttpClientModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: environment.keyGoogleMaps,
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [AppDataService,LabelsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
