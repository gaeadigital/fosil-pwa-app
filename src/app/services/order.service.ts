import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Order } from '../entity/order';
import { HttpClient } from '@angular/common/http';
import { Setting } from '../entity/setting';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  orderCollectionName = 'orders';
  settingsCollectionName = 'settings';

  constructor(private afs: AngularFirestore, private httpClient:HttpClient) { }


  async saveOrder(order: Order) {

    if (order.id === null) {
      order.id = this.afs.createId();
      console.log(order.id);
    }

    const data = {
      address: order.address,
      phoneNumber: order.phoneNumber,
      paymentType: order.paymentType,
      paymentStatus: order.paymentStatus,
      status: order.status,
      totalAmount: order.totalAmount,
      deliveryAmount: order.deliveryAmount,
      deliveryDistance: order.deliveryDistance,
      timestampReceived: order.timestampReceived,
      timestampRecord: order.timestampRecord,
      order: order.order,
      customerPickUp:order.customerPickUp,
      to:order.to,
      cc:order.cc,
      message:order.message
    }

    return await this.afs.collection(this.orderCollectionName).doc(order.id).set(data, { merge: true });
  }

  async requestPaymentGateway(order: Order){

    const params = {
      'MERCHANTNUMBER':order.id.substr(0,10),
      'OPERATION':order.id,
      'ORDERNUMBER': order.orderNumber,
      'AMOUNT':order.totalAmount + order.deliveryAmount,
      'DEPOSITFLAG':1,
      'URL':'https://fosil-pwa-test.web.app',
      'DIGEST':0
    };
    
    return await this.httpClient.post<any>('https://test.3dsecure.gpwebpay.com/pgw/order.do', params).subscribe({
        next: data => {
            console.log(data);
        },
        error: error => {
            console.error('There was an error!', error);
        }
    });
  }

  async getSettings() { 
    return await this.afs.collection<Setting>(this.settingsCollectionName).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Setting;
        const id = a.payload.doc.id;
        return { id, ...data };
      })));
  }
}
