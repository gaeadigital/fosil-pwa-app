import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseIngredientsComponent } from './choose-ingredients.component';

describe('ChooseIngredientsComponent', () => {
  let component: ChooseIngredientsComponent;
  let fixture: ComponentFixture<ChooseIngredientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseIngredientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseIngredientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
