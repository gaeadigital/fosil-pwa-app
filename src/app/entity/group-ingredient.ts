import { Observable } from 'rxjs';
import { Ingredient } from './ingredient';

export class GroupIngredient {

    id?:string = null;
    detail:string = null;
    label?:string = null;
    order?:number = null;

    ingredients?:Array<Ingredient>
}
